from django.shortcuts import render, redirect
from .models import User
from usuarios.forms import UsuarioForm
from django.contrib.auth.hashers import make_password
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required


def registrar_usuario(request):
    if (request.method == 'POST'):
        form = UsuarioForm(request.POST)
        if form.is_valid():
            usuario = form.save()
            usuario.password = make_password(usuario.password)
            usuario.save()
            return redirect('/entrar')
    else:
        form = UsuarioForm()
    return render(request, 'registrar_usuario.html', {'formulario': form, 'accion': 'Registrar Usuario'})


@login_required
def modificar_usuario(request):
    usuario = User.objects.get(username=request.user.username)
    if request.method == "POST":
        form = UsuarioForm(request.POST, instance=usuario)
        if form.is_valid():
            usuario = form.save()
            usuario.password = make_password(usuario.password)
            usuario.save()
            logout(request)
            return redirect('/entrar')
    else:
        form = UsuarioForm(instance=usuario)
    print(form)
    return render(request, 'registrar_usuario.html', {'formulario': form, 'accion': 'Modificar Perfil'})


@login_required
def ver_perfil(request):
    id = request.user.id
    usuario = User.objects.get(pk=id)
    return render(request, 'ver_perfil.html', {'usuario': usuario})


def lista_blogs(request):
    usuarios = User.objects.all()
    return render(request, 'blog_list.html', {'usuarios': usuarios})


@login_required
def suscribir(request):
    pass
