from django.urls import path
from usuarios.views import registrar_usuario, ver_perfil, modificar_usuario, lista_blogs, suscribir

urlpatterns = [
    path(r'', lista_blogs, name='lista_blogs'),
    path(r'registrar', registrar_usuario, name='registrar_usuario'),
    path(r'perfil', ver_perfil, name='ver_perfil'),
    path(r'modificar', modificar_usuario, name='modificar_usuario'),
    path(r'suscribir', suscribir, name='suscribir'),
]
