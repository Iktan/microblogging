from django import forms
from .models import User


class UsuarioForm(forms.models.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'first_name', 'last_name')
