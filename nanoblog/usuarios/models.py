from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    follow = models.ManyToManyField(
        'self', related_name='followers', symmetrical=False)
