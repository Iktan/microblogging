from django.test import TestCase
from .models import User
from usuarios.forms import UsuarioForm


class usuariosTest(TestCase):

    def test_verPaginaBlogList(self):
        response = self.client.get('/user/')
        self.assertEquals(response.status_code, 200)

    def test_verificarTemplateBlogList(self):
        response = self.client.get('/user/')
        self.assertTemplateUsed(response, 'blog_list.html')

    def test_verPaginaPerfil(self):
        usuario = User(
            username='Pepe_Relo',
            email='pepe_relo@nanoblog.com',
            first_name='Pepe',
            last_name='Relo'
        )
        usuario.set_password = 'peperelo4'
        usuario.save()
        self.client.force_login(usuario)
        response = self.client.get('/user/perfil')
        self.assertEquals(response.status_code, 200)

    def test_verificarTemplatePerfil(self):
        usuario = User(
            username='Pepe_Relo',
            email='pepe_relo@nanoblog.com',
            first_name='Pepe',
            last_name='Relo'
        )
        usuario.set_password = 'peperelo4'
        usuario.save()
        self.client.force_login(usuario)
        response = self.client.get('/user/perfil')
        self.assertTemplateUsed(response, 'ver_perfil.html')

    def test_verPerfilRedirectNoLogin(self):
        response = self.client.get('/user/perfil')
        self.assertRedirects(response, '/entrar?next=/user/perfil')

    def test_verPaginaRegistrarUsuario(self):
        response = self.client.get('/user/registrar')
        self.assertEquals(response.status_code, 200)

    def test_verificarTemplateRegistrarUsuario(self):
        response = self.client.get('/user/registrar')
        self.assertTemplateUsed(response, 'registrar_usuario.html')

    def test_registrarUsuario(self):
        data = {
            'username': 'pepe_relo',
            'email': 'pepe_relo@test.com',
            'password': 'pepe_relo',
            'first_name': 'pepe',
            'last_name': 'relo'
        }
        response = self.client.post('/user/registrar', data=data,)
        self.assertRedirects(response, '/entrar')

    def test_verPaginaModificarPerfil(self):
        usuario = User(
            username='Pepe_Relo',
            email='pepe_relo@nanoblog.com',
            first_name='Pepe',
            last_name='Relo'
        )
        usuario.set_password = 'peperelo4'
        usuario.save()
        self.client.force_login(usuario)
        response = self.client.get('/user/modificar')
        self.assertEquals(response.status_code, 200)

    def test_verificarTemplateModificarPerfil(self):
        usuario = User(
            username='Pepe_Relo',
            email='pepe_relo@nanoblog.com',
            first_name='Pepe',
            last_name='Relo'
        )
        usuario.set_password = 'peperelo4'
        usuario.save()
        self.client.force_login(usuario)
        response = self.client.get('/user/modificar')
        self.assertTemplateUsed(response, 'registrar_usuario.html')

    def test_modificarUsuario(self):
        usuario = User.objects.create_user(
            username='Pepe_Relo',
            email='pepe_relo@nanoblog.com',
            password='peperelo4',
            first_name='Pepe',
            last_name='Relo'
        )
        data = {
            'username': usuario.username,
            'email': usuario.username,
            'password': usuario.username,
            'first_name': usuario.username,
            'last_name': usuario.username
        }

        self.client.force_login(usuario)
        new_data = {
            'username': usuario.username,
            'email': 'pepito_481@msn.com',
            'password': 'peperelo1',
            'first_name': usuario.username,
            'last_name': usuario.username
        }
        response = self.client.post(
            '/user/modificar', data=new_data, follow=True)
        self.assertRedirects(response, '/entrar')

    def test_modificarPerfilRedirectNoLogin(self):
        response = self.client.get('/user/modificar')
        self.assertRedirects(response, '/entrar?next=/user/modificar')

    def test_guardar_usuario_form(self):
        User.objects.create_user(
            username='Pepe_Relo',
            email='pepe_relo@nanoblog.com',
            password='peperelo4',
            first_name='Pepe',
            last_name='Relo'
        )
        form = UsuarioForm(
            data={
                'username': User.username,
                'email': User.username,
                'password': User.username,
                'first_name': User.username,
                'last_name': User.username
            }
        )
        if form.is_valid():
            new_usuario = form.save()
            self.assertEqual(new_usuario, User.objects.latest('id'))
