"""nanoblog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from login.urls import login_nano, logout_nano
from blog.urls import timeline, repost


urlpatterns = [
    path(r'', login_nano, name='index'),
    path(r'posts/', include('blog.urls')),
    path(r'user/', include('usuarios.urls'), name='usuarios'),
    path(r'admin/', admin.site.urls),
    path(r'entrar', login_nano, name='login_nano'),
    path(r'salir', logout_nano, name='logout_nano'),
    path('timeline', timeline, name='timeline'),
    path('repostear', repost, name='repost'),
]
