Característica: Ver Seguidores
    Como usuario de nanoblog
    Quiero ver mis seguidores de nanoblog
    Para saber quienes siguen mis nanoblogs

    Escenario: Ver mis seguidores
        Dado que tengo una sesión iniciada como Pepe_Relo
        Y entro a la página de Inicio
        Cuando doy clic en Seguidores
        Entonces puedo ver que estoy en la página de Seguidores