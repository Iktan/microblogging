Característica: Ver Seguidos
    Como usuario de nanoblog
    Quiero ver a quien sigo en nanoblog
    Para saber a quienes sigo

    Escenario: Ver a quien sigo
        Dado que tengo una sesión iniciada como Pepe_Relo
        Y entro a la página de Inicio
        Cuando doy clic en Seguidos
        Entonces puedo ver que estoy en la página de Seguidos