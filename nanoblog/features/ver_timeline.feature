Característica: Ver timeline
    Como usuario de nanoblog
    Quiero ver mi timeline
    Para ver los nanoblogs que me interesan

    Escenario: Ver timeline como usuario
        Dado que tengo una sesión iniciada
        Cuando entro a la página de Inicio
        Entonces puedo ver que hay nanoblogs en mi timeline
    
    Escenario: Ver timeline como usuario no registrado
        Dado que no tengo una sesión iniciada
        Cuando entro a la página de usuarios
        Entonces puedo ver que hay nombres de nanoblogs en el timeline