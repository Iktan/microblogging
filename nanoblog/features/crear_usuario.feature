Característica: Crear usuario
    Como potencial usuario de nanoblog
    Quiero crear un usuario
    Para poder hacer microblogs

    Escenario: Crear usuario
        Dado que estoy en la página de inicio de sesión
        Cuando doy clic en Registrarme
        Y introduzco "Terry_Senpai" como nombre de usuario del nuevo usuario
        Y introduzco "terry_senpai@nanoblog.com" como correo electronico del nuevo usuario
        Y introduzco "terry_s3npa1" como contraseña del nuevo usuario
        Y introduzco "Terry" como nombre del nuevo usuario
        Y introduzco "Crews" como apellido del nuevo usuario
        Y doy clic en Crear Cuenta
        Entonces puedo ver que estoy en la página de Inicio de Sesión
        Y puedo ver que sale un mensaje que dice "Usuario creado correctamente. Ahora puedes iniciar sesión"
    
    Escenario: Crear usuario sin llenar el formulario
        Dado que estoy en la página de inicio de sesión
        Cuando doy clic en Registrarme
        Y doy clic en Crear Cuenta
        Entonces puedo ver que estoy en la página de Crear Usuario
        Y puedo ver que sale un mensaje que dice "Los campos son obligatorios."
