Característica: Iniciar sesión
    Como usuario de nanoblog
    Quiero iniciar sesión
    Para poder ver los microblogs

    Escenario: Iniciar sesión con credenciales válidas
        Dado que estoy en la página de inicio de sesión
        Cuando introduzco "Pepe_Relo" como nombre de usuario de inicio de sesión
        Y introduzco "p3p3r3l0" como contraseña de inicio de sesión
        Y doy clic en Iniciar Sesión
        Entonces puedo ver que estoy en la página de Inicio
    
    Escenario: Iniciar sesión con credenciales inválidas
        Dado que estoy en la página de inicio de sesión
        Cuando introduzco "No_es_usuario" como nombre de usuario de inicio de sesión
        Y introduzco "invalid0" como contraseña de inicio de sesión
        Y doy clic en Iniciar Sesión
        Entonces puedo ver que sale un mensaje que dice "Credenciales inválidas" en la página de inicio de sesión
