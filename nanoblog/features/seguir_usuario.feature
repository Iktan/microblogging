Característica: Seguir Usuario
    Como usuario de nanoblog
    Quiero seguir a otro usuario de nanoblog
    Para ver sus nanoblogs en mi inicio

    Escenario: Seguir Usuario
        Dado que tengo una sesión iniciada como Pepe_Relo
        Y entro al perfil de Terry_Senpai
        Cuando doy clic en Seguir
        Entonces puedo ver que sale un mensaje que dice "¡Ahora sigues a este usuario!"