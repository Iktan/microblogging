Característica: Mandar Microblog
    Como usuario de nanoblog
    Quiero mandar un microblog
    Para escribir lo que estoy pensando en menos de 140 caracteres

    Escenario: Escribir un microblog
        Dado que tengo una sesión iniciada
        Y entro a la página de Inicio
        Cuando escribo "¡Hoy es un buen día para morir!" como mensaje
        Y doy clic en Postear
        Entonces puedo ver que se ha posteado "¡Hoy es un buen día para morir!" como mensaje