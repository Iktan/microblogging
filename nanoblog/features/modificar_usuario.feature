Característica: Modificar usuario
    Como usuario de nanoblog
    Quiero modificar mi usuario
    Para cambiar los datos de mi cuenta

    Escenario: Modificar usuario
        Dado que tengo una sesión iniciada
        Y entro a la página de Mi Perfil
        Cuando doy clic en Modificar Perfil
        Y introduzco "Terry_Senpai" como nuevo nombre de usuario
        Y introduzco "the_terry_senpai@nanoblog.com" como nuevo correo electronico
        Y introduzco "terry_s3npa1" como nueva contraseña 
        Y introduzco "Terrence" como nuevo nombre
        Y introduzco "Crews" como nuevo apellido
        Y doy clic en Modificar Usuario
        Entonces puedo ver que estoy en la página de Mi Perfil
        Y puedo ver que sale un mensaje que dice "Usuario modificado correctamente."

    Escenario: Modificar usuario dejando en blanco un campo
        Dado que tengo una sesión iniciada
        Y entro a la página de Mi Perfil
        Cuando doy clic en Modificar Perfil
        Y dejo en blanco el campo de nombre de usuario
        Y doy clic en Modificar Usuario
        Entonces puedo ver que estoy en la página de Modificar Perfil
        Y puedo ver que sale un mensaje que dice "Los campos son obligatorios."