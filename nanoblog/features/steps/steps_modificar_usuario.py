from behave import given, when, then
from django.conf import settings
from selenium import webdriver
import time


@given(u'entro a la página de Mi Perfil')
def step_impl(context):
    time.sleep(1)
    context.browser.get('http://localhost:8000/user/perfil')


@when(u'doy clic en Modificar Perfil')
def step_impl(context):
    time.sleep(3)
    context.browser.find_element_by_link_text('Editar').click()


@when(u'introduzco "{usuario}" como nuevo nombre de usuario')
def step_impl(context, usuario):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_username')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(usuario)


@when(u'introduzco "{email}" como nuevo correo electronico')
def step_impl(context, email):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_email')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(email)


@when(u'introduzco "{contraseña}" como nueva contraseña')
def step_impl(context, contraseña):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_password')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(contraseña)


@when(u'introduzco "{nombre}" como nuevo nombre')
def step_impl(context, nombre):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_first_name')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(nombre)


@when(u'introduzco "{apellido}" como nuevo apellido')
def step_impl(context, apellido):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_last_name')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(apellido)


@when(u'doy clic en Modificar Usuario')
def step_impl(context):
    context.browser.find_element_by_xpath(
        "//button[text()='"+'Modificar Perfil'+"']").click()


@then(u'puedo ver que estoy en la página de Mi Perfil')
def step_impl(context):
    assert context.browser.current_url == 'http://localhost:8000/entrar'


@when(u'dejo en blanco el campo de nombre de usuario')
def step_impl(context):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_username')
    time.sleep(1)
    name.clear()


@then(u'puedo ver que estoy en la página de Modificar Perfil')
def step_impl(context):
    assert context.browser.current_url == 'http://localhost:8000/user/modificar'
