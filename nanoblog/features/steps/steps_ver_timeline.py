from behave import given, when, then
from django.conf import settings
from selenium import webdriver
import time


@when(u'entro a la página de Inicio')
def step_impl(context):
    context.browser.get('http://localhost:8000/timeline')


@then(u'puedo ver que hay nanoblogs en mi timeline')
def step_impl(context):
    assert context.browser.current_url == 'http://localhost:8000/timeline'


@given(u'que no tengo una sesión iniciada')
def step_impl(context):
    context.browser.get('http://localhost:8000/salir')


@when(u'entro a la página de usuarios')
def step_impl(context):
    context.browser.get('http://localhost:8000/user/')


@then(u'puedo ver que hay nombres de nanoblogs en el timeline')
def step_impl(context):
    assert context.browser.current_url == 'http://localhost:8000/user/'
