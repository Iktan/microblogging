from behave import when, then
from django.conf import settings
from selenium import webdriver
import time


@when(u'doy clic en Registrarme')
def step_impl(context):
    context.browser.find_element_by_link_text('registrar').click()


@when(u'introduzco "{usuario}" como nombre de usuario del nuevo usuario')
def step_impl(context, usuario):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_username')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(usuario)


@when(u'introduzco "{email}" como correo electronico del nuevo usuario')
def step_impl(context, email):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_email')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(email)


@when(u'introduzco "{contraseña}" como contraseña del nuevo usuario')
def step_impl(context, contraseña):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_password')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(contraseña)


@when(u'introduzco "{nombre}" como nombre del nuevo usuario')
def step_impl(context, nombre):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_first_name')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(nombre)


@when(u'introduzco "{apellido}" como apellido del nuevo usuario')
def step_impl(context, apellido):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_last_name')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(apellido)


@when(u'doy clic en Crear Cuenta')
def step_impl(context):
    time.sleep(3)
    context.browser.find_element_by_xpath(
        "//button[text()='"+'Registrar Usuario'+"']").click()


@then(u'puedo ver que estoy en la página de Inicio de Sesión')
def step_impl(context):
    assert context.browser.current_url == 'http://localhost:8000/entrar'


@then(u'puedo ver que estoy en la página de Crear Usuario')
def step_impl(context):
    assert context.browser.current_url == 'http://localhost:8000/user/registrar'
