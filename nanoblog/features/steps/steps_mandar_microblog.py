from behave import when, then
from django.conf import settings
from selenium import webdriver
import time


@when(u'introduzco "{textoPost}" como mensaje')
def step_impl(context, nanoblog):
    context.browser.find_element_by_xpath(
        '/html/body/table/tbody/tr/td[1]/form/div/input[1]').sendKeys(context.textoPost)
    time.sleep(2)


@when(u'escribo "{textoPost}" como mensaje')
def step_impl(context):
    context.browser.find_element_by_xpath(
        '/html/body/table/tbody/tr/td[1]/form/div/input[1]').sendKeys(context.textoPost)
    time.sleep(2)


@when(u'doy clic en Postear')
def step_impl(context):
    context.browser.find_element_by_xpath(
        '/html/body/table/tbody/tr/td[1]/form/div/input[2]').click()
    time.sleep(2)


@then(u'puedo ver que se ha posteado "{textoPost}" como mensaje')
def step_impl(context):
    table = context.browser.find_element_by_xpath('//table[2]')
    for row in table:
        temp = row
        if temp == context.textoPost:
            assert True
            break
        else:
            assert False
