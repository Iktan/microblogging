from behave import given, then
from django.conf import settings
from selenium import webdriver
import time


@given(u'que estoy en la página de inicio de sesión')
def step_impl(context):
    context.browser.get('http://localhost:8000/entrar')


@then(u'puedo ver que estoy en la página de Inicio')
def step_impl(context):
    time.sleep(5)
    assert context.browser.current_url == 'http://localhost:8000/timeline'
    context.browser.get('http://localhost:8000/salir')


@then(u'puedo ver que sale un mensaje que dice "{mensaje}"')
def step_impl(context, mensaje):
    pass


@given(u'que tengo una sesión iniciada')
def step_impl(context):
    context.browser.get('http://localhost:8000/salir')
    time.sleep(3)
    name = context.browser.find_element_by_name('username')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys('Terry_Senpai')
    time.sleep(3)
    name = context.browser.find_element_by_name('password')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys('terry_s3npa1')
    context.browser.find_element_by_xpath(
        "//button[text()='"+'Sign In'+"']").click()


@given(u'entro a la página de Inicio')
def step_impl(context):
    context.browser.get('http://localhost:8000/timeline')


@given(u'que tengo una sesión iniciada como Pepe_Relo')
def step_impl(context):
    pass
