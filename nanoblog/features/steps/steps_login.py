from behave import when, then
from django.conf import settings
from selenium import webdriver
import time


@when(u'introduzco "{usuario}" como nombre de usuario de inicio de sesión')
def step_impl(context, usuario):
    time.sleep(3)
    name = context.browser.find_element_by_name('username')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(usuario)


@when(u'introduzco "{contraseña}" como contraseña de inicio de sesión')
def step_impl(context, contraseña):
    time.sleep(3)
    name = context.browser.find_element_by_name('password')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(contraseña)


@when(u'doy clic en Iniciar Sesión')
def step_impl(context):
    context.browser.find_element_by_xpath(
        "//button[text()='"+'Sign In'+"']").click()


@then(u'puedo ver que sale un mensaje que dice "Credenciales inválidas" en la página de inicio de sesión')
def step_impl(context):
    assert context.browser.current_url == 'http://localhost:8000/entrar'
