# CONTAINS: Browser fixture setup and teardown
from behave import fixture, use_fixture
from selenium.webdriver import Firefox
import time


@fixture
def browser_firefox(context):
    # -- BEHAVE-FIXTURE: Similar to @contextlib.contextmanager
    context.browser = Firefox()
    yield context.browser
    # -- CLEANUP-FIXTURE PART:
    context.browser.quit()


def before_all(context):
    use_fixture(browser_firefox, context)
    context.browser.get('http://localhost:8000/user/registrar')
    time.sleep(4)
    name = context.browser.find_element_by_id('id_username')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys('Pepe_Relo')
    time.sleep(2)
    name = context.browser.find_element_by_id('id_email')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys('pepito@gmail.com')
    time.sleep(2)
    name = context.browser.find_element_by_id('id_password')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys('p3p3r3l0')
    time.sleep(2)
    name = context.browser.find_element_by_id('id_first_name')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys('Iktan')
    time.sleep(3)
    name = context.browser.find_element_by_id('id_last_name')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys('Diaz')
    context.browser.find_element_by_xpath(
        "//button[text()='"+'Registrar Usuario'+"']").click()


def after_all(context):
    context.browser.quit()
