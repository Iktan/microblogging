from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from blog.models import Post


# Create your views here.

@login_required
def timeline(request):
    if request.user.is_authenticated:
        post_list = Post.objects.order_by('fecha')
        template_name = "post_list.html"

        context = {
            "post_list": post_list
        }

        return render(request, template_name, context)
    else:
        return redirect('/user/')


@login_required
def post(request):
    if not request.POST:
        return HttpResponseRedirect('/timeline')
    p = request.POST.get('post_content')

    post = Post(autor=request.user, post=p)
    post.save()

    return timeline(request)


@login_required
def repost(request):
    '''
    Repostea un post creado por otro usuario.
    '''
    if not request.POST:
        return HttpResponseRedirect('/timeline')

    original = request.POST.get('post_id')
    if not original:
        messages.error(request, 'Algo malo ocurrió D:!')
        return HttpResponseRedirect('/timeline')

    original = Post.objects.get(id=original)

    repost = Post(autor=request.user, post='',
                  repost=True, original_post=original)

    repost.save()
    messages.success(request, '¡Post reblogueado!')

    return HttpResponseRedirect('/timeline')


@login_required
def buscar(request):
    if not request.POST:
        return HttpResponseRedirect('timeline')

    busqueda = request.POST.get('busqueda')

    resultados = Post.objects.filter(
        Q(autor__username__contains=busqueda) | Q(post__contains=busqueda))

    template_name = "post_list.html"

    context = {
        "post_list": resultados
    }

    return render(request, template_name, context)


@login_required
def buscar_reposts(request):
    mis_posts = Post.objects.filter(autor=request.user)

    rs = []
    for p in mis_posts:
        reps = p.compartidos.all()
        rs += reps

    template_name = "post_list.html"

    context = {
        "post_list": rs
    }

    return render(request, template_name, context)
