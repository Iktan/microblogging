from django.urls import path
from blog.views import timeline, repost, post, buscar, buscar_reposts

urlpatterns = [
    path(r'timeline', timeline, name='timeline'),
    path(r'repostear', repost, name='repost'),
    path(r'nuevo_post', post, name='nuevo_post'),
    path(r'buscar', buscar, name='buscar'),
    path(r'reposts', buscar_reposts, name='reposts')
]
