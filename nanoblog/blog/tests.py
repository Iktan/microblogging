from usuarios.models import User
from django.test import TestCase
from .models import Post

from django.shortcuts import reverse

from django.contrib.auth.hashers import make_password


class BlogTests(TestCase):
    '''
    Pruebas correspondientes a la funcionalidad extra
    '''

    def setUp(self):
        self.usr1 = User(
            username='test_1',
            password=make_password('puerko1234'),
            email='test@test.com'
        )
        self.usr1.save()

        self.usr2 = User(
            username='test_2',
            password=make_password('puerko1234'),
            email='test@test.com'
        )
        self.usr2.save()

    def test_login(self):
        response = self.client.login(username='test_1', password='puerko1234')

        self.assertEqual(response, True)

    def test_get_timeline(self):
        self.client.force_login(self.usr1)

        response = self.client.get('/timeline')

        self.assertEqual(len(response.context['post_list']), 0)

    def test_new_post(self):
        self.client.force_login(self.usr1)
        response = self.client.get('/timeline')

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase!!'})

        self.assertEqual(len(response.context['post_list']), 1)

    def test_new_many_post(self):
        self.client.force_login(self.usr1)
        response = self.client.get('/timeline')

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 1!!'})

        self.client.logout()

        self.client.force_login(self.usr2)

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 2!!'})

        self.assertEqual(len(response.context['post_list']), 8)

    def test_repost(self):
        self.client.force_login(self.usr1)
        response = self.client.get('/timeline')

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 1!!'})

        self.client.logout()

        self.client.force_login(self.usr2)

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 2!!'})

        response = self.client.post(reverse(
            'repost'), {'post_id': response.context['post_list'][4].id}, follow=True)

        self.assertEqual(len(response.context['post_list']), 9)

    def test_buscar_posts_by_content(self):
        self.client.force_login(self.usr1)
        response = self.client.get('/timeline')

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 1!!'})

        self.client.logout()

        self.client.force_login(self.usr2)

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 2!!'})

        response = self.client.post(
            reverse('buscar'), {'busqueda': 'TestCase 2'})

        self.assertEqual(len(response.context['post_list']), 2)

    def test_buscar_posts_by_content_by_content2(self):
        self.client.force_login(self.usr1)
        response = self.client.get('/timeline')

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 1!!'})

        self.client.logout()

        self.client.force_login(self.usr2)

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 2!!'})

        response = self.client.post(
            reverse('buscar'), {'busqueda': 'usuario 2'})

        self.assertEqual(len(response.context['post_list']), 4)

    def test_buscar_posts_by_user(self):
        self.client.force_login(self.usr1)
        response = self.client.get('/timeline')

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 1!!'})

        self.client.logout()

        self.client.force_login(self.usr2)

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 2!!'})

        response = self.client.post(reverse('buscar'), {'busqueda': 'test_1'})

        self.assertEqual(len(response.context['post_list']), 4)

    def test_buscar_posts_by_user_2(self):
        self.client.force_login(self.usr1)
        response = self.client.get('/timeline')

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 1!!'})

        self.client.logout()

        self.client.force_login(self.usr2)

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 2!!'})

        response = self.client.post(reverse('buscar'), {'busqueda': 'test_'})

        self.assertEqual(len(response.context['post_list']), 8)

    def test_mostrar_reposted(self):
        self.client.force_login(self.usr1)
        response = self.client.get('/timeline')

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 1!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 1!!'})

        self.client.logout()

        self.client.force_login(self.usr2)

        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 1 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 2 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 3 como usuario 2!!'})
        response = self.client.post(reverse('nuevo_post'), {
            'post_content': 'Este es mi post desde TestCase 4 como usuario 2!!'})

        response = self.client.post(reverse(
            'repost'), {'post_id': response.context['post_list'][4].id}, follow=True)

        response = self.client.get(reverse('reposts'), follow=True)

        self.assertEqual(len(response.context['post_list']), 1)

    def test_redirectTimelineNoLogin(self):
        response = self.client.get('/posts/timeline')
        self.assertRedirects(response, '/entrar?next=/posts/timeline')

    def tearDown(self):
        self.usr1.delete()
        self.usr2.delete()


class BlogTest2(TestCase):

    def setUp(self):
        self.us1 = User.objects.create(username='puerkito66')
        self.us2 = User.objects.create(username='puerkito')
        self.us3 = User.objects.create(username='puerkito99')
        self.us4 = User.objects.create(username='puerkito88')
        self.us5 = User.objects.create(username='puerkito77')
        self.us6 = User.objects.create(username='puerkito33')

        self.post1 = Post(
            texto='Hola, Este es un texto 1!!', autor=self.us1)
        self.post1.save()
        self.post2 = Post(
            texto='Hola, Este es un texto 2!!', autor=self.us1)
        self.post2.save()
        self.post3 = Post(
            texto='Hola, Este es un texto 3!!', autor=self.us1)
        self.post3.save()
        self.post4 = Post(
            texto='Hola, Este es un texto 4!!', autor=self.us1)
        self.post4.save()
        self.post5 = Post(
            texto='Hola, Este es un texto 5!!', autor=self.us1)
        self.post5.save()
        self.post6 = Post(
            texto='Hola, Este es un texto 6!!', autor=self.us1)
        self.post6.save()
