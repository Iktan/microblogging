# -*- coding: utf8 -*-

from django.db import models

from usuarios.models import User


class Post(models.Model):
    '''
    Define los post en sí, los datos de quién postea y el post original.
    '''
    autor = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='entradas')
    post = models.CharField(max_length=140)
    fecha = models.DateField(auto_now_add=True)

    # Define si es un repost. Si no lo es, la llave foranea debería ser vacía.
    repost = models.BooleanField(default=False)
    original_post = models.ForeignKey(
        'self', null=True, related_name='compartidos', on_delete=models.CASCADE)

    class Meta:
        ordering = ['-fecha']

    def __str__(self):
        if self.repost:
            _original = Post.objects.get(id=self.original_post.id)
            return 'reposteado por {} --> \'{}:\n\t{}\''.format(self.autor.username,
                                                                _original.autor.username, _original.post)
        else:
            return '{}:\n\t{}'.format(self.autor.username, self.post)
