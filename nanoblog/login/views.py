from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout


def login_nano(request):
    if request.user.is_authenticated:
        return redirect('posts/timeline')

    if request.method == 'POST':
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/timeline')
        else:
            return render(request, 'page-signin.html', {'error': 'Usuario no válido'})
    return render(request, 'page-signin.html')


def logout_nano(request):
    logout(request)
    return redirect('/entrar')
