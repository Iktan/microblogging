from django.test import TestCase
from usuarios.models import User


class TestLogin(TestCase):
    def test_uses_login_template(self):
        response = self.client.get('/entrar')
        self.assertTemplateUsed(response, 'page-signin.html')

    def test_verPagina_login(self):
        response = self.client.get('/entrar')
        self.assertEquals(response.status_code, 200)

    def test_inicio_sesion(self):
        User.objects.create_user(
            username='usuario', password='password', email='user@gmail.com')
        response = self.client.login(
            username='usuario', password='password')
        self.assertEqual(response, True)

    def test_login_redireccion_sesion_iniciada(self):
        User.objects.create_user(
            username='usuario', password='password', email='user@gmail.com')
        self.client.login(
            username='usuario', password='password')
        response = self.client.get('/entrar')
        self.assertRedirects(response, '/posts/timeline')

    def test_inicio_sesion_post(self):
        usuario = User.objects.create_user(
            username='usuario', password='password', email='user@gmail.com')
        data = {
            'username': usuario.username,
            'password': usuario.password
        }
        response = self.client.post('/entrar', data=data)
        self.assertEqual(response.status_code, 200)

    def test_login_redireccion_sesion_iniciada_post(self):
        usuario = User.objects.create_user(
            username='usuario', password='password', email='user@gmail.com')
        data = {
            'username': usuario.username,
            'password': 'password'
        }
        response = self.client.post('/entrar', data=data)
        self.assertRedirects(response, '/timeline')

    def test_cierre_sesion(self):
        User.objects.create_user(
            username='usuario', password='password', email='user@gmail.com')
        self.client.login(
            username='usuario', password='password')
        response = self.client.get('/salir')
        self.assertEqual(response.status_code, 302)

    def test_inicio_sesion_credenciales_invalidas(self):
        User.objects.create_user(
            username='usuario', password='password', email='user@gmail.com')
        data = {
            'username': 'user',
            'password': 'pass'
        }
        response = self.client.post('/entrar', data=data)
        self.assertTemplateUsed(response, 'page-signin.html')
