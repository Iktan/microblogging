from django.urls import path
from login.views import login_nano, logout_nano

urlpatterns = [
    path('entrar', login_nano, name='login_nano'),
    path('salir', logout_nano, name='logout_nano'),
]
